/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duplication

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCasePost(t *testing.T) {
	store.InitializeTestDatabase()
	params := url.Values{}
	params.Add(httputil.TestCaseName, "new name")
	params.Add(httputil.Version, "1")

	invalidVersionParams := url.Values{}
	invalidVersionParams.Add(httputil.TestCaseName, "new name")
	invalidVersionParams.Add(httputil.Version, "-1")

	shortNameParams := url.Values{}
	shortNameParams.Add(httputil.TestCaseName, "a")
	shortNameParams.Add(httputil.Version, "-1")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.ProjectKey:  handler.DummyProject,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxNoCase := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
			middleware.ProjectKey:  handler.DummyProject,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No case in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleRequest(ctxNoCase, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoCase, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No testcase",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(ctxNoCase, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Invalid version",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(ctx, http.MethodPost, invalidVersionParams),
		),
		handler.CreateTest("Name to short",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(ctx, http.MethodPost, shortNameParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(a, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, params),
		),
	)
}
