// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

const (
	sequenceVersionField = "test_sequence_version"
	caseField            = "test_case"
)

type sequenceVersionCaseRow struct {
	Id                  int64
	TestSequenceVersion int64
	TestCase            int64
	CaseIndex           int
}

func saveSequenceVersionTestCases(s xorm.Interface, svrID int64, tcs []test.Case) error {
	var svcrs []sequenceVersionCaseRow
	for i, c := range tcs {
		crID, err := lookupCaseRowID(s, c.ID())
		if err != nil {
			return err
		}

		svcr := sequenceVersionCaseRow{TestSequenceVersion: svrID, TestCase: crID, CaseIndex: i}
		svcrs = append(svcrs, svcr)
	}

	return insertSequenceVersionCaseRows(s, svcrs...)
}

func insertSequenceVersionCaseRows(s xorm.Interface, svcrs ...sequenceVersionCaseRow) error {
	_, err := s.Table(sequenceVersionCaseTable).Insert(&svcrs)
	return err
}

func listSequenceVersionCases(s xorm.Interface, pID id.ProjectID, vrID int64) ([]test.Case, error) {
	crs, err := listCaseRowsForSequenceVersion(s, vrID)
	if err != nil {
		return nil, err
	}

	var cases []test.Case
	for _, cr := range crs {
		var c *test.Case
		var ex bool
		c, ex, err = buildCase(s, pID, cr)
		if err != nil {
			return nil, err
		}

		if ex {
			cases = append(cases, *c)
		}
	}

	return cases, err
}

func listCaseRowsForSequenceVersion(s xorm.Interface, vrID int64) ([]caseRow, error) {
	var ids []int64
	err := s.Table(sequenceVersionCaseTable).Cols(caseField).In(sequenceVersionField, vrID).Asc(caseIndexField).Find(&ids)

	var crs []caseRow
	for _, id := range ids {
		var cr caseRow
		var ex bool
		cr, ex, err = getCaseRowByID(s, id)

		if err != nil {
			return nil, err
		}

		if ex {
			crs = append(crs, cr)
		}
	}

	return crs, err
}

func getCaseRowByID(s xorm.Interface, id int64) (caseRow, bool, error) {
	cr := caseRow{}
	ex, err := s.Table(caseTable).ID(id).Get(&cr)

	if err != nil || !ex {
		return caseRow{}, false, err
	}

	return cr, true, nil
}
