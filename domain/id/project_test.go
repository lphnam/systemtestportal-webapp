/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"reflect"
	"testing"
)

func TestNewProjectID(t *testing.T) {
	owner := NewActorID("testUser")
	pNames := []string{
		"Project",
		"Project 1",
		"Project-1",
		"",
		"   ",
		"§%&%§",
	}
	for _, pName := range pNames {
		id := NewProjectID(owner, pName)
		if !reflect.DeepEqual(owner, id.ActorID) {
			t.Errorf("Owner wasn't saved correctly to the new ProjectID. Expected %v but got %v.",
				owner, id.Owner())
		}
		if pName != id.Project() {
			t.Errorf("Projectname wasn't saved correctly to new ProjectID. Expected %q but got %q.",
				pName, id.Project())
		}
	}
}

type ProjectExistenceCheckerStub struct {
	exists bool
	err    error
}

func (pc ProjectExistenceCheckerStub) Exists(ProjectID) (bool, error) {
	return pc.exists, pc.err
}
func TestProjectID_Validate(t *testing.T) {
	owner := NewActorID("owners name")
	testData := []struct {
		name          string
		returnExists  bool
		returnError   error
		errorExpected bool
	}{
		{"projectName", false, nil, false},
		{"tre", false, nil, true},
		{"", false, nil, true},
		{"&$(=§$", false, nil, false},
		{"thisNameAlreadyExists", true, nil, true},
		{"name", false, fmt.Errorf("i'm an error"), true},
		{"This character is forbidden: /", false, nil, true},
		{"This character is forbidden: \\", false, nil, true},
		{"This character is forbidden: ?", false, nil, true},
		{"This character is forbidden: #", false, nil, true},
	}
	for _, set := range testData {
		id := NewProjectID(owner, set.name)
		result := id.Validate(ProjectExistenceCheckerStub{set.returnExists, set.returnError})
		if set.errorExpected && (result == nil) {
			t.Errorf("Validate returned no error, but expected one. \nID: %+v", id)
		}
		if !set.errorExpected && (result != nil) {
			t.Errorf("Validate returned an error, but expected none. \nID: %+v \nError: %v", id, result)
		}
	}
}
