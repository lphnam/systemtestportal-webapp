/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package httputil

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"path"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"golang.org/x/text/language"
)

// The parameter keys of a request
const (
	// User
	Identifier  = "inputIdentifier"
	Password    = "inputPassword"
	DisplayName = "inputDisplayName"
	UserName    = "inputUserName"
	Email       = "inputEmail"
	IsAdmin     = "inputIsAdmin"

	// Project
	ProjectName        = "inputProjectName"
	ProjectDescription = "inputProjectDesc"
	ProjectImage       = "inputProjectLogo"
	ProjectVisibility  = "optionsProjectVisibility"
	ProjectRoles       = "inputProjectRoles"

	Version = "version"

	// TestCases
	TestCaseName = "inputTestCaseName"
	TestVersion  = "testVersion"

	// TestSequences
	TestSequenceName          = "inputTestSequenceName"
	TestSequenceDescription   = "inputTestSequenceDescription"
	TestSequencePreconditions = "inputTestSequencePreconditions"
	TestSequenceTestCase      = "inputTestSequenceTestCase"
	NewTestCases              = "newTestcases"
	CommitMessage             = "inputCommitMessage"
	IsMinor                   = "isMinor"
	TestSequenceLabels        = "inputLabels"

	// Members
	Members = "members"
	Roles   = "roles"

	Member = "member"
	Role   = "role"

	// Assignment
	NewTesters  = "newtesters"
	Versions    = "versions"
	Variants    = "variants"
	DeleteTasks = "doneTasks" // The tasks to delete

	// Protocols
	ProtocolType        = "type"
	SelectedProtocol    = "selected"
	PreconditionResults = "preconditions"
	ProtocolNr          = "protocolNr"

	// Group
	GroupName        = "inputGroupName"
	GroupDescription = "inputGroupDesc"
	GroupVisibility  = "inputGroupVisibility"

	// Activity

	OffsetCount = "offsetCount"

	// Comment
	CommentId   = "commentId"
	CommentText = "commentText"
	Fragment    = "fragment"

	// Settings
	IsAccessAllowed                 = "isAccessAllowed"
	IsRegistrationAllowed           = "isRegistrationAllowed"
	IsEmailVerification             = "isEmailVerification"
	IsDeleteUsers                   = "isDeleteUsers"
	IsDisplayMessage                = "isDisplayMessage"
	IsGlobalMessageExpirationDate   = "isGlobalMessageExpirationDate"
	IsExecutionTime                 = "isExecutionTime"
	GlobalMessage                   = "globalMessage"
	GlobalMessageType               = "globalMessageType"
	GlobalMessageTimeStamp          = "globalMessageTimeStamp"
	GlobalMessageExpirationDate     = "globalMessageExpirationDate"
	GlobalMessageExpirationDateUnix = "globalMessageExpirationDateUnix"
	ImprintMessage                  = "imprintMessage"
	PrivacyMessage                  = "privacyMessage"

	//Uploads
	UploadSource = "uploadSrc"
	UploadName   = "uploadName"

	//Import
	Import = "inputImport"

	//Label
	LabelId        = "labelId"
	LabelDesc      = "labelDesc"
	LabelColor     = "labelColor"
	LabelTextColor = "labelTextColor"
	LabelName      = "labelName"

	// Integrations in gitlab etc
	GitlabIsActive     = "inputIsActive"
	GitlabProjectID    = "inputProjectID"
	GitlabProjectToken = "inputProjectToken"
)

// DumpResponse is like DumpRequest but for responses.
func DumpResponse(r *http.Response) string {
	d, err := httputil.DumpResponse(r, true)
	if err != nil {
		return fmt.Sprintf("%+v", r)
	}
	return string(d)
}

// DumpRequest dumps given request into printable string.
func DumpRequest(r *http.Request) string {
	d, err := httputil.DumpRequest(r, true)
	if err != nil {
		return fmt.Sprintf("%+v", r)
	}
	return string(d)
}

// IsFragmentRequest checks if the request contains a parameter "fragment".
// If the parameter is one of 1, t, T, TRUE, true or True the function returns true,
// else it returns false
func IsFragmentRequest(r *http.Request) bool {
	if r == nil {
		return false
	}
	if frag := r.FormValue(Fragment); frag != "" {
		isFrag, err := strconv.ParseBool(frag)
		if err == nil {
			return isFrag
		}
	}
	return false
}

// GetLanguageFromRequest reads the Accept-Language header from a request. If the language is not supported, the default en-US will be returned.
func GetLanguageFromRequest(r *http.Request) string {
	matcher := language.NewMatcher([]language.Tag{
		language.AmericanEnglish,
		language.German,
		language.Make("de-DE"),
		language.English,
	})

	accept := r.Header.Get("Accept-Language")
	tag, _ := language.MatchStrings(matcher, accept)

	return tag.String()
}

// GenerateShaForFile returns the sha256 for a given file with a sha256-" prefix. The file has to exist, else "error" is returned.
func GenerateShaForFile(file string) string {
	path := path.Join(config.Get().BasePath, file)
	body, err := ioutil.ReadFile(path)

	if err != nil {
		return ""
	}

	hash := sha256.Sum256(body)
	sha := base64.StdEncoding.EncodeToString(hash[:])

	return fmt.Sprintf("sha256-%s", sha)
}
