/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

// The Sequence struct contains information for describing a test sequence
type Sequence struct {
	Id               int64
	CreatedAt 	time.Time `xorm:"created"`

	// Foreign Key ----------------|
	ProjectId        int64
	// ----------------------------|

	Name             string

	Project          id.ProjectID      `xorm:"-"`
	Labels           []*project.Label   `xorm:"-"`
	SequenceVersions []SequenceVersion `xorm:"-"`
}

// SequenceVersion contains information for describing a version of a test sequence
type SequenceVersion struct {
	VersionNr     int
	Message       string
	IsMinor       bool
	Description   string
	Preconditions []Precondition
	CreationDate  time.Time
	Cases         []Case
	SequenceInfo  SequenceInfo
	Testsequence  id.TestID
}

// SequenceInfo contains meta information for a SequenceVersion.
// This data depends on the cases that the sequence contains.
type SequenceInfo struct {
	Versions      map[string]*project.Version
	DurationHours int
	DurationMin   int
}

// ItemEstimatedDuration returns the estimated Duration for the sequence
func (tsv SequenceVersion) ItemEstimatedDuration() (minutes int, hours int) {
	minutes = 0
	hours = 0

	for _, c := range tsv.Cases {
		minutes += c.TestCaseVersions[0].Duration.GetMinuteInHour()
		hours += int(c.TestCaseVersions[0].Duration.Hours())
	}
	return
}

// Rename renames this sequence also changing it's id.
func (ts *Sequence) Rename(n string) {
	ts.Name = n
}

// ItemName returns a test cases name
func (ts Sequence) ItemName() string {
	return ts.Name
}

// CountIncludedSteps returns the total number of steps summed over all included cases
func (tsv SequenceVersion) CountIncludedSteps() int {
	result := 0
	for _, tc := range tsv.Cases {
		result += len(tc.TestCaseVersions[0].Steps)
	}
	return result
}

// CountIncludedStepsUpTo returns the total number of steps summed over all included cases up to maxCaseNr
// the numeration starts with 0 and maxCaseNr itself will be included
func (tsv SequenceVersion) CountIncludedStepsUpTo(maxCaseNr int) int {
	result := 0
	for i := 0; i <= maxCaseNr && i < len(tsv.Cases); i++ {
		tcv := tsv.Cases[i].TestCaseVersions[0]
		result += len(tcv.Steps)
	}
	return result
}

// ID returns a test sequences id
func (ts Sequence) ID() id.TestID {
	return id.NewTestID(ts.Project, ts.Name, false)
}

// ID returns a test sequences id
func (tsv SequenceVersion) ID() id.TestVersionID {
	return id.NewTestVersionID(tsv.Testsequence, tsv.VersionNr)
}

// NewTestSequence creates a new test sequence with the given information
func NewTestSequence(name, description string, preconditions []Precondition,
	cases []Case, project id.ProjectID) (Sequence, error) {

	name = strings.TrimSpace(name)

	ts := Sequence{
		Name:    name,
		Project: project,
	}

	initVer := 1
	initMessage := "Initial test sequence created"

	tsv, err := NewTestSequenceVersion(initVer, false, initMessage, description, preconditions, cases, ts.ID())

	ts.SequenceVersions = append(ts.SequenceVersions, tsv)

	return ts, err
}

// NewTestSequenceVersion creates a new version for a test case
// Returns the test case the version belongs to
func NewTestSequenceVersion(version int, isMinor bool, message, description string, preconditions []Precondition,
	cases []Case, testsequence id.TestID) (SequenceVersion, error) {

	description = strings.TrimSpace(description)
	message = strings.TrimSpace(message)

	sutVersions, err := CalculateSUTVariants(cases)
	if err != nil {
		return SequenceVersion{}, err
	}

	dur := CalculateDuration(cases)

	si := SequenceInfo{
		Versions:      sutVersions,
		DurationHours: int(dur.Hours()),
		DurationMin:   dur.GetMinuteInHour(),
	}

	tsv := SequenceVersion{
		VersionNr:     version,
		Message:       message,
		IsMinor:       isMinor,
		Description:   description,
		Preconditions: preconditions,
		CreationDate:  time.Now().UTC().Round(time.Second),
		Cases:         cases,
		SequenceInfo:  si,
		Testsequence:  testsequence,
	}

	return tsv, nil
}

//CalculateSUTVariant calculates the applicable sut variants
func CalculateSUTVariants(cases []Case) (map[string]*project.Version, error) {
	var applicableVersions = map[string]*project.Version{}

	// Check if cases is not empty
	// Check if the newest version of the first test case really contains versions
	if len(cases) > 0 && len(cases[0].TestCaseVersions[0].Versions) > 0 {

		// Add all versions of the first test case to the list with applicable variants
		for _, version := range cases[0].TestCaseVersions[0].Versions {
			applicableVersions[version.Name] = version
		}

		// Iterate over all test cases to check all versions of them
		for _, tc := range cases {
			// If a test case contains no variants, remove all variants from the applicableVersions list
			if len(tc.TestCaseVersions[0].Versions) <= 0 {
				applicableVersions = map[string]*project.Version{}
			} else {
				checkVersionForApplicableVariants(applicableVersions, tc)
			}
		}
	}

	return applicableVersions, nil
}

// checkVersionForApplicableVariants calculates the cutset of the test cases (tc) versions
// and test applicableVersions
func checkVersionForApplicableVariants(applicableVersions map[string]*project.Version, tc Case) {
	for verIndex, version := range applicableVersions {
		if _, ok := tc.TestCaseVersions[0].Versions[verIndex]; !ok {
			delete(applicableVersions, verIndex)
			continue
		}

		// track deleted elements to adjust index
		deleteCounter := 0
		// Check all variants of a version
		for variantIndex, variant := range version.Variants {
			// If the version does not contain the variant remove the variant from the
			// version in the applicableVersions list
			if !ContainsVariant(tc.TestCaseVersions[0].Versions[verIndex].Variants, variant) {
				applicableVersions[verIndex].Variants = append(
					applicableVersions[verIndex].Variants[:variantIndex-deleteCounter],
					applicableVersions[verIndex].Variants[variantIndex-deleteCounter+1:]...)
				// element deleted, adjust index since variantIndex will be off!
				deleteCounter = deleteCounter + 1
			}
		}
	}
}

// containsVariantchecks if the variantList contains the given variant
func ContainsVariant(variantList []project.Variant, variant project.Variant) bool {
	for _, vari := range variantList {
		if vari.Name == variant.Name {
			return true
		}
	}
	return false
}

// CalculateDuration calculates the durationHours and durationMin for testsequences
func CalculateDuration(cases []Case) duration.Duration {
	var result = duration.Duration{Duration: 0}
	var tcv = CaseVersion{}
	for _, c := range cases {
		tcv = c.TestCaseVersions[0]
		result = result.Add(tcv.Duration)
	}
	return result
}

// UpdateInfo updates the SequenceInfo of a SequenceVersion, populating it with generated data
func UpdateInfo(ts *SequenceVersion) (*SequenceVersion, error) {
	cases := ts.Cases
	sutVersions, err := CalculateSUTVariants(cases)

	if err != nil {
		return ts, err
	}

	dur := CalculateDuration(cases)

	si := SequenceInfo{
		Versions:      sutVersions,
		DurationHours: int(dur.Hours()),
		DurationMin:   dur.GetMinuteInHour(),
	}

	ts.SequenceInfo = si

	return ts, nil
}
