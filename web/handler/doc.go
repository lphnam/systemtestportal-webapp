/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package handler contains the logic of the server.

It contains sub-packages for the different use-cases (creating, editing, deleting...).
The handler package contains interfaces for the sub-packages
to access the store (store_interfaces.go).
The handler package contains functions for the sub-packages
to retrieve templates from the template package (template.go).
The handler package contains functions for the sub-packages
to access data from the context (context.go).

Testing the handlers:
The handler packages provides functions, mocks and dummy-data for the sub-packages to test the handlers.
The functions are in the files starting with testing_*.go
*/
package handler
