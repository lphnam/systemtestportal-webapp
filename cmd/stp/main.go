/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/nicksnyder/go-i18n/i18n"
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/routing"
)

func main() {
	if err := config.Load(); err != nil {
		panic(err)
	}
	addr := fmt.Sprintf("%s:%d", config.Get().Host, config.Get().Port)
	if config.Get().Debug {
		log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)
	}
	store.InitializeDatabase()
	r := routing.InitRouter()
	i18n.MustLoadTranslationFile("static/i18n/en-us.all.json")
	i18n.MustLoadTranslationFile("static/i18n/de-DE.all.json")

	log.Printf("Listening on %s", addr)
	err := http.ListenAndServe(addr, r)
	if err != nil {
		log.Fatal("ListenAndServe error: ", err)
	}
}
