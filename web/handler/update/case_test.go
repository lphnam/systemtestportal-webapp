/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCasePut(t *testing.T) {
	store.InitializeTestDatabase()
	body := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":[\"Cond\"]," +
		"\"inputTestCaseSUTVariants\":{\"Var\":{\"Name\":\"Var\",\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[{\"ID\":0,\"actual\":\"Step\",\"expected\":\"Result\"}]" +
		"}" +
		"}"

	// Body with same values as DummyTestCase except this body contains
	// sut-versions. The DummyTestCase does not contain versions.
	bodyNewVersions := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test Case 1\"," +
		"\"inputTestCaseDescription\":\"First Test Case\"," +
		"\"inputTestCasePreconditions\":[]," +
		"\"inputTestCaseSUTVariants\":{\"Var\":{\"Name\":\"Var\",\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":0," +
		"\"inputMinutes\":0," +
		"\"inputSteps\":[{\"ID\":0,\"actual\":\"Step\",\"expected\":\"Result\"}]" +
		"}" +
		"}"
	// Body with same values as the DummyTestCaseSUTVersions except this body
	// has different sut-versions.
	bodyDifferentVersions := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test Case 1\"," +
		"\"inputTestCaseDescription\":\"First Test Case\"," +
		"\"inputTestCasePreconditions\":[]," +
		"\"inputTestCaseSUTVariants\":" +
		"{\"Chrome\":{\"Name\":\"Chrome\"," +
		"\"Versions\":[{\"Name\":\"1.0.1\"},{\"Name\":\"2.0.1\"},{\"Name\":\"3.0.5\"}]}," +
		"\"Firefox\":{\"Name\":\"Firefox\"," +
		"\"Versions\":[{\"Name\":\"1.0.1\"},{\"Name\":\"2.0.1\"},{\"Name\":\"3.0.5\"}]}," +
		"\"Microsoft Edge\":{\"Name\":\"Microsoft Edge\"," +
		"\"Versions\":[{\"Name\":\"1.0.2\"},{\"Name\":\"2.0.2\"},{\"Name\":\"3.0.10\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":0," +
		"\"inputMinutes\":0," +
		"\"inputSteps\":[{\"ID\":0,\"actual\":\"Step\",\"expected\":\"Result\"}]" +
		"}" +
		"}"

	noStepsBody := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":[\"Cond\"]," +
		"\"inputTestCaseSUTVariants\":{\"Var\":{\"Name\":\"Var\",\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[]" +
		"}" +
		"}"

	bodyRename := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test Case 2\"," +
		"\"inputTestCaseDescription\":\"First Test Case\"," +
		"\"inputTestCasePreconditions\":[]," +
		"\"inputTestCaseSUTVariants\":{}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":0," +
		"\"inputMinutes\":0," +
		"\"inputSteps\":[{\"ID\":0,\"actual\":\"Step\",\"expected\":\"Result\"}]" +
		"}" +
		"}"
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)
	// This context contains a test case without sut-versions
	ctx := map[interface{}]interface{}{
		middleware.TestCaseKey: handler.DummyTestCase,
		middleware.ProjectKey:  handler.DummyProject,
		middleware.UserKey:     handler.DummyUser,
	}
	// This context contains a case with sut-versions
	ctxCaseSUTVersion := map[interface{}]interface{}{
		middleware.TestCaseKey: handler.DummyTestCaseSUTVersions,
		middleware.ProjectKey:  handler.DummyProject,
		middleware.UserKey:     handler.DummyUser,
	}
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.TestCaseKey: handler.DummyTestCaseSUTVersions,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)
	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}
				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 0),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("No project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 0),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 0),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("No test steps",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 1),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 1),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, noStepsBody),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, noStepsBody),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 1),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 1),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, body),
		),
		// Test updating a case that had no sut-versions.
		// After editing the case has sut-versions.
		// The sut-versions are the only information that changed,
		// so no new case-version is created.
		handler.CreateTest("Add sut-version to case without sut-version",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 1),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 0),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, bodyNewVersions),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, bodyNewVersions),
		),
		// Test changing the sut-versions of an existing case
		handler.CreateTest("Change sut-versions of case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 1),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 0),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
					handler.HasCalls(taskAssigneeGetter, 0),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctxCaseSUTVersion), http.MethodPut, handler.NoParams,
				bodyDifferentVersions),
			handler.NewFragmentRequest(handler.SimpleContext(ctxCaseSUTVersion), http.MethodPut, handler.NoParams,
				bodyDifferentVersions),
		),
		// Test changing the name of an existing case
		handler.CreateTest("Change name of a case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseUpdaterMock := &handler.CaseUpdaterMock{}
				testExistenceCheckerMock := &handler.TestExistenceCheckerMock{}
				commentStoreMock := &handler.CommentStoreMock{}
				taskAssigneeGetter := &handler.TaskGetterMock{}
				testCaseListerMock := &handler.CaseListerMock{}

				return CasePut(caseUpdaterMock, testExistenceCheckerMock, commentStoreMock, taskAssigneeGetter, testCaseListerMock), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&caseUpdaterMock.CaseAdderMock, 1),
					handler.HasCalls(&caseUpdaterMock.CaseDeleterMock, 0),
					handler.HasCalls(&caseUpdaterMock.CaseRenamerMock, 1),
					handler.HasCalls(&commentStoreMock.CommentGetterMock, 0),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams,
				bodyRename),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams,
				bodyRename),
		),
	)
}
