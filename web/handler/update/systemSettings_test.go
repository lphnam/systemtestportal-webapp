/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"net/url"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

func TestSystemSettingsPut(t *testing.T) {

	params := url.Values{}
	params.Add(httputil.IsAccessAllowed, "true")
	params.Add(httputil.IsRegistrationAllowed, "false")
	params.Add(httputil.IsEmailVerification, "false")
	params.Add(httputil.IsDisplayMessage, "true")
	params.Add(httputil.IsExecutionTime, "true")
	params.Add(httputil.IsGlobalMessageExpirationDate, "true")
	params.Add(httputil.GlobalMessage, "thisIsTheGlobalMessage")
	params.Add(httputil.GlobalMessageType, "trololol")
	params.Add(httputil.GlobalMessageTimeStamp, time.Now().String())
	params.Add(httputil.GlobalMessageExpirationDate, "12/27/2010")
	params.Add(httputil.GlobalMessageExpirationDateUnix, "1231231231231")
	params.Add(httputil.IsDeleteUsers, "true")

	systemSettingsStoreMock := handler.SystemSettingsStoreMock{}

	tested := SystemSettingsPut
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested(systemSettingsStoreMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(handler.EmptyCtx, http.MethodPut, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)

	params.Set(httputil.IsAccessAllowed, "notabool")
	tested = SystemSettingsPut
	handler.Suite(t,
		handler.CreateTest("Invalid boolean: Access Allowed",
			handler.ExpectResponse(
				tested(systemSettingsStoreMock),
				handler.HasStatus(http.StatusBadRequest),
			),
			handler.SimpleRequest(handler.EmptyCtx, http.MethodPut, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)

	params.Set(httputil.IsAccessAllowed, "true")
	params.Set(httputil.GlobalMessage, "")
	tested = SystemSettingsPut
	handler.Suite(t,
		handler.CreateTest("Empty Message",
			handler.ExpectResponse(
				tested(systemSettingsStoreMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(handler.EmptyCtx, http.MethodPut, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)

	params.Set(httputil.GlobalMessageExpirationDate, "")
	tested = SystemSettingsPut
	handler.Suite(t,
		handler.CreateTest("Empty Expiration Date",
			handler.ExpectResponse(
				tested(systemSettingsStoreMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(handler.EmptyCtx, http.MethodPut, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)

	params.Set(httputil.GlobalMessageType, "")
	tested = SystemSettingsPut
	handler.Suite(t,
		handler.CreateTest("Empty global message type",
			handler.ExpectResponse(
				tested(systemSettingsStoreMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(handler.EmptyCtx, http.MethodPut, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)
}
