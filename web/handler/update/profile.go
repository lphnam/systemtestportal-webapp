/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package update

import (
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

func EditProfilePut(ul handler.UserLister, userUpdater handler.UserUpdater) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		requestedUserName := r.FormValue("user")
		var targetUser *user.User = nil
		for _, user := range users {
			if user.Name == requestedUserName {
				targetUser = user
			}
		}

		if currentUser != nil && targetUser != nil && err == nil {
			if targetUser.ID() == currentUser.ID() {
				invalidFields := []string{}

				email := r.FormValue("email")
				bio := r.FormValue("bio")

				isShownPublic, err := strconv.ParseBool(r.FormValue("isShownPublic"))
				if err != nil {
					invalidFields = append(invalidFields, "IsShownPublic is not a boolean")
				}

				isEmailPublic, err := strconv.ParseBool(r.FormValue("isEmailPublic"))
				if err != nil {
					invalidFields = append(invalidFields, "IsEmailPublic is not a boolean")
				}

				var projectHeaderMode int32
				mode := r.FormValue("projectHeaderMode")
				if mode == "full" {
					projectHeaderMode = 1
				} else if mode == "compact" {
					projectHeaderMode = 2
				} else if mode == "hidden" {
					projectHeaderMode = 3
				} else {
					projectHeaderMode = 1
				}

				image := r.FormValue("profilePicture")

				targetUser.Email = email
				targetUser.Biography = bio
				targetUser.IsShownPublic = isShownPublic
				targetUser.IsEmailPublic = isEmailPublic
				targetUser.ProjectHeaderMode = int32(projectHeaderMode)

				imagePath, err := storeuploads.WriteProfilePictureToFile(image, *targetUser)
				if err != nil {
					errors.Handle(err, w, r)
					return
				}
				targetUser.Image = imagePath

				err = userUpdater.Update(targetUser)
				if err == nil && len(invalidFields) == 0 {
					w.WriteHeader(200)
				} else {
					var responseText string

					responseText = responseText + "Invalid Fields:"
					for i := 0; i < len(invalidFields); i++ {
						responseText = responseText + "\n" + " - " + invalidFields[i]
					}
					responseText = responseText + "\n\n"

					errors.Handle(errors.ConstructStd(http.StatusBadRequest, "Bad Request", responseText, r).Finish(), w, r)
				}

			} else {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}
		} else {
			if currentUser == nil {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			} else {
				errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
				return
			}
		}
	}
}
func EditPasswordPut(ul handler.UserLister, ua handler.UserAdder, uv handler.UserValidator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		password := r.FormValue("userPassword")
		requestedUserName := r.FormValue("user")

		authUser, valid, err := uv.Validate(requestedUserName, password)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		var targetUser *user.User = nil
		for _, user := range users {
			if user.Name == requestedUserName {
				targetUser = user
			}
		}

		if currentUser != nil && targetUser != nil && authUser != nil && err == nil {
			if targetUser.ID() == currentUser.ID() && currentUser.ID() == authUser.ID() && valid {
				invalidFields := []string{}

				newPassword := r.FormValue("newPassword")

				user := user.PasswordUser{
					User:     *targetUser,
					Password: newPassword,
				}
				err = ua.Add(&user)
				if err == nil && len(invalidFields) == 0 {
					w.WriteHeader(200)
				} else {
					var responseText string

					responseText = responseText + "Invalid Fields:"
					for i := 0; i < len(invalidFields); i++ {
						responseText = responseText + "\n" + " - " + invalidFields[i]
					}
					responseText = responseText + "\n\n"

					errors.Handle(errors.ConstructStd(http.StatusBadRequest, "Bad Request", responseText, r).Finish(), w, r)
				}

			} else {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}
		} else {
			if currentUser == nil {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			} else {
				errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
				return
			}
		}
	}

}

// DeactivateUserPut deletes or deactivates a user, identified by the user an password form values,
// depending on the system settings attained through contextdomain.GetGlobalSystemSettings().IsDeleteUsers
func DeactivateUserPut(uu handler.UserUpdater, uv handler.UserValidator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		password := r.FormValue("password")
		requestedUserName := r.FormValue("user")

		authUser, valid, err := uv.Validate(requestedUserName, password)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		if !valid {
			errors.Handle(errors.ConstructStd(http.StatusUnauthorized, "Authorization Failed", "The provided user name or password is incorrect.", r).Finish(), w, r)
			return
		}
		if currentUser != nil && err == nil {
			if authUser.ID() == currentUser.ID() && valid {
				isDelete := contextdomain.GetGlobalSystemSettings().IsDeleteUsers

				err := uu.Deactivate(authUser, isDelete)

				if err == nil {
					w.WriteHeader(200)
				} else {
					errors.Handle(errors.ConstructStd(http.StatusBadRequest, "Bad Request", err.Error(), r).Finish(), w, r)
				}

			} else {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}
		} else {
			if currentUser == nil {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			} else {
				errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
				return
			}
		}
	}

}
