// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
)

const (
	errorNoProject     = "no project with name %#v"
	errorNoProjectByID = "no project with the id %#v"
)

type ProjectsSQL struct {
	e *xorm.Engine
}

type projectRow struct {
	ID           int64
	OwnerID      int64
	Name         string
	Description  string
	Image        string
	Token        string
	Visibility   int
	CreationDate time.Time

	GitlabIsActive bool
	GitlabID       string
	GitlabToken    string
}

func projectFromRow(pr projectRow) *project.Project {
	return &project.Project{
		Id:           pr.ID,
		OwnerId:      pr.OwnerID,
		Name:         pr.Name,
		Description:  pr.Description,
		Image:        pr.Image,
		Token:        pr.Token,
		Visibility:   visibility.Visibility(pr.Visibility),
		CreationDate: pr.CreationDate,
		Integrations: project.Integrations{
			GitlabProject: project.GitlabProject{
				IsActive: pr.GitlabIsActive,
				ID:       pr.GitlabID,
				Token:    pr.GitlabToken,
			},
		},
	}
}

func rowFromProject(p *project.Project) projectRow {
	return projectRow{
		ID:           p.Id,
		OwnerID:      p.OwnerId,
		Name:         p.Name,
		Description:  p.Description,
		Image:        p.Image,
		Token:        p.Token,
		Visibility:   int(p.Visibility),
		CreationDate: p.CreationDate,

		GitlabIsActive: p.Integrations.GitlabProject.IsActive,
		GitlabID:       p.Integrations.GitlabProject.ID,
		GitlabToken:    p.Integrations.GitlabProject.Token,
	}
}

// ListForOwner lists all projects where the given actor is the owner
func (psSQL ProjectsSQL) ListForOwner(owner id.ActorID) ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listProjectsForOwner(s, id.NewActorID(owner.Actor()))
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}
	sortProjects(ps)

	return ps, s.Commit()
}

// ListForMember returns all projects where the given actor is a member
func (psSQL ProjectsSQL) ListForMember(member id.ActorID) ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listProjectsForMember(s, member)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}
	sortProjects(ps)

	return ps, s.Commit()
}

// ListForActor returns all projects the actor has access to
func (psSQL ProjectsSQL) ListForActor(actor id.ActorID) ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listProjectsForActor(s, actor)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}
	sortProjects(ps)

	return ps, s.Commit()
}

// ListAll lists all projects in the system
func (psSQL ProjectsSQL) ListAll() ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listAllProjects(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}
	sortProjects(ps)

	return ps, s.Commit()
}

// ListPublic lists all public projects
func (psSQL ProjectsSQL) ListPublic() ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listPublicProjects(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return ps, s.Commit()
}

// ListInternal lists all internal projects
func (psSQL ProjectsSQL) ListInternal() ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listInternalProjects(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return ps, s.Commit()
}

// ListPrivate lists all private projects where the given actor is the owner
func (psSQL ProjectsSQL) ListPrivate(actor id.ActorID) ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listPrivateProjects(s, actor)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return ps, s.Commit()
}

func (psSQL ProjectsSQL) Add(p *project.Project) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := saveProject(s, p)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (psSQL ProjectsSQL) Get(pID id.ProjectID) (*project.Project, bool, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, false, err
	}

	p, ex, err := getProject(s, pID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, false, err
	}

	return p, ex, s.Commit()
}

func (psSQL ProjectsSQL) Exists(pID id.ProjectID) (bool, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return false, err
	}

	_, ex, err := getProjectRow(s, pID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return false, err
	}
	return ex, s.Commit()
}

func (psSQL ProjectsSQL) Delete(pID id.ProjectID) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	if err := deleteProject(s, pID); err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func saveProject(s xorm.Interface, p *project.Project) error {
	orID, err := lookupOwnerRowID(s, p.Owner)
	if err != nil {
		return err
	}

	npr := rowFromProject(p)
	npr.OwnerID = orID

	oprID, ex, err := getProjectRowID(s, p.ID())
	if err != nil {
		return err
	}

	if ex {
		npr.ID = oprID
		err = updateProjectRow(s, &npr)
	} else {
		err = insertProjectRow(s, &npr)

		// Set the rows Id to the project for usage
		p.Id = npr.ID
	}

	if err != nil {
		return err
	}

	err = saveSUTVersions(s, npr.ID, p.Versions)
	if err != nil {
		return err
	}

	err = saveProjectRoles(s, npr.ID, p.Roles)
	if err != nil {
		return err
	}

	err = saveProjectMembers(s, npr, p.UserMembers)

	npr.Image = p.Image

	return err
}

func updateProjectRow(s xorm.Interface, pr *projectRow) error {
	// AllCols() is needed because boolean columns are not updated by default
	aff, err := s.Table(projectTable).ID(pr.ID).AllCols().Update(pr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertProjectRow(s xorm.Interface, pr *projectRow) error {
	_, err := s.Table(projectTable).Insert(pr)
	return err
}

func deleteProject(s xorm.Interface, pID id.ProjectID) error {
	pr, ex, err := getProjectRow(s, pID)
	if err != nil {
		return err
	}
	if !ex {
		return fmt.Errorf(errorNoProject, pID.Project())
	}

	pr.CreationDate = pr.CreationDate.Round(time.Second).UTC()

	_, err = s.Table("activity").Delete(&activity.Activity{ProjectID: pr.ID})
	if err != nil {
		return err
	}
	_, err = s.Table(projectTable).Delete(&pr)
	if err != nil {
		return err
	}

	if pr.Image != "" {
		err := storeuploads.DeleteImage(pr.Image, storeuploads.ImageType.Project)
		if err != nil {
			return err
		}
	}
	return nil
}

func listProjectsForOwner(s xorm.Interface, aID id.ActorID) ([]*project.Project, error) {
	orID, err := lookupOwnerRowID(s, aID)
	if err != nil {
		return nil, err
	}

	prs, err := listProjectRowsForOwner(s, orID)
	if err != nil {
		return nil, err
	}

	ps, err := buildProjects(s, prs)

	if err != nil {
		return nil, err
	}

	return ps, err
}

func listProjectsForMember(s xorm.Interface, member id.ActorID) ([]*project.Project, error) {
	orID, err := lookupOwnerRowID(s, member)
	if err != nil {
		return nil, err
	}

	prs, err := listProjectRowsForMember(s, orID)
	if err != nil {
		return nil, err
	}

	ps, err := buildProjects(s, prs)

	if err != nil {
		return nil, err
	}

	return ps, err
}

func listProjectsForActor(s xorm.Interface, actor id.ActorID) ([]*project.Project, error) {
	orID, err := lookupOwnerRowID(s, actor)
	if err != nil {
		return nil, err
	}

	var actorProjectRows []projectRow
	// List public projects
	publicProjects, err := listPublicProjectRows(s)
	if err != nil {
		return nil, err
	}
	actorProjectRows = append(actorProjectRows, publicProjects...)
	// List internal projects
	internalProjects, err := listInternalProjectRows(s)
	if err != nil {
		return nil, err
	}
	actorProjectRows = append(actorProjectRows, internalProjects...)
	// List projects where actor is member
	memberProjectRows, err := listProjectRowsForMember(s, orID)
	if err != nil {
		return nil, err
	}
	actorProjectRows = append(actorProjectRows, memberProjectRows...)

	// Remove duplicates from list.
	// Some projects might be public/internal but the actor
	// is also a member.
	actorProjectRows = removeDuplicatesFromProjectRows(actorProjectRows)

	actorProjects, err := buildProjects(s, actorProjectRows)
	if err != nil {
		return nil, err
	}

	return actorProjects, err
}

func listAllProjects(s xorm.Interface) ([]*project.Project, error) {
	ors, err := listOwnerRows(s)
	if err != nil {
		return nil, err
	}

	var ps []*project.Project
	for _, or := range ors {
		var prs []projectRow
		prs, err = listProjectRowsForOwner(s, or.ID)
		if err != nil {
			return nil, err
		}

		var p []*project.Project
		p, err = buildProjects(s, prs)
		if err != nil {
			return nil, err
		}

		ps = append(ps, p...)
	}
	sortProjects(ps)

	return ps, nil
}

func listPublicProjects(s xorm.Interface) ([]*project.Project, error) {
	var prs []projectRow
	prs, err := listPublicProjectRows(s)
	if err != nil {
		return nil, err
	}

	ps, err := buildProjects(s, prs)
	if err != nil {
		return nil, err
	}

	sortProjects(ps)

	return ps, nil
}

func listInternalProjects(s xorm.Interface) ([]*project.Project, error) {
	var prs []projectRow
	prs, err := listInternalProjectRows(s)
	if err != nil {
		return nil, err
	}

	ps, err := buildProjects(s, prs)
	if err != nil {
		return nil, err
	}

	sortProjects(ps)

	return ps, nil
}

// listInternalProjectRows lists all internal project rows
func listInternalProjectRows(s xorm.Interface) ([]projectRow, error) {
	ors, err := listOwnerRows(s)
	if err != nil {
		return nil, err
	}

	var projectRows []projectRow
	for _, or := range ors {
		prs, err := listInternalProjectRowsForOwner(s, or.ID)
		if err != nil {
			return nil, err
		}
		projectRows = append(projectRows, prs...)
	}
	return projectRows, nil
}

func listPublicProjectRows(s xorm.Interface) ([]projectRow, error) {
	ors, err := listOwnerRows(s)
	if err != nil {
		return nil, err
	}

	var projectRows []projectRow
	for _, or := range ors {
		prs, err := listPublicProjectRowsForOwner(s, or.ID)
		if err != nil {
			return nil, err
		}
		projectRows = append(projectRows, prs...)
	}
	return projectRows, nil
}

func listPrivateProjects(s xorm.Interface, actor id.ActorID) ([]*project.Project, error) {
	var ps []*project.Project
	var prs []projectRow

	// Get id of actor
	ownerID, ex, err := getOwnerRowID(s, actor)
	if err != nil {
		return []*project.Project{}, err
	}
	if !ex {
		return []*project.Project{}, fmt.Errorf(errorNoOwnerByName, actor)
	}

	prs, err = listPrivateProjectRows(s, ownerID)
	if err != nil {
		return nil, err
	}

	var p []*project.Project
	p, err = buildProjects(s, prs)
	if err != nil {
		return nil, err
	}

	ps = append(ps, p...)

	sortProjects(ps)

	return ps, nil
}

func getProject(s xorm.Interface, pID id.ProjectID) (*project.Project, bool, error) {
	pr, ex, err := getProjectRow(s, pID)
	if err != nil || !ex {
		return nil, false, err
	}

	p, err := buildProject(s, pr)
	if err != nil {
		return nil, false, err
	}

	p.Owner = pID.ActorID

	return p, true, nil
}

func buildProjects(s xorm.Interface, prs []projectRow) ([]*project.Project, error) {
	var ps []*project.Project
	for _, pr := range prs {
		p, err := buildProject(s, pr)
		if err != nil {
			return nil, err
		}

		owner, err := getOwnerRowByID(s, pr.OwnerID)
		if err != nil {
			return nil, err
		}

		p.Owner = owner
		ps = append(ps, p)
	}

	return ps, nil
}

func buildProject(s xorm.Interface, pr projectRow) (*project.Project, error) {
	var err error
	labelStore := GetLabelStore()

	project := projectFromRow(pr)

	project.Versions, err = listSUTVersions(s, pr.ID)
	if err != nil {
		return nil, err
	}

	project.Labels, err = labelStore.GetLabelsForProject(project)
	if err != nil {
		return nil, err
	}

	project.UserMembers, err = listProjectMemberships(s, pr.ID)
	if err != nil {
		return nil, err
	}

	project.Roles, err = listProjectRoles(s, pr.ID)
	if err != nil {
		return nil, err
	}

	return project, nil
}

func lookupProjectRowID(s xorm.Interface, pID id.ProjectID) (int64, error) {
	prID, ex, err := getProjectRowID(s, pID)

	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoProject, pID.Project())
	}

	return prID, nil
}

func getProjectRowID(s xorm.Interface, pID id.ProjectID) (int64, bool, error) {
	pr, ex, err := getProjectRow(s, pID)
	return pr.ID, ex, err
}

// listProjectRowsForOwner returns all project rows of the given owner
func listProjectRowsForOwner(s xorm.Interface, orID int64) ([]projectRow, error) {
	var prs []projectRow
	err := s.Table(projectTable).Find(&prs, &projectRow{OwnerID: orID})
	if err != nil {
		return nil, err
	}

	return prs, nil
}

// listProjectRowsForMember returns all projects rows where the given memberID is member
func listProjectRowsForMember(s xorm.Interface, memberID int64) ([]projectRow, error) {
	// Get all memberships for the memberID
	userMemberShipRows, err := listUserMemberShipsForMember(s, memberID)
	if err != nil {
		return nil, err
	}

	// Get projectRows from these userMemberShipRows
	var projectRows []projectRow
	for _, userMemberShipRow := range userMemberShipRows {
		projectRow, ex, err := getProjectRowByID(s, userMemberShipRow.ProjectID)
		if !ex {
			return nil, fmt.Errorf(errorNoProjectByID, userMemberShipRow.ProjectID)
		}
		if err != nil {
			return nil, err
		}
		projectRows = append(projectRows, projectRow)
	}

	return projectRows, nil
}

// listPublicProjectRowsForOwner returns the public project rows of the given owner
func listPublicProjectRowsForOwner(s xorm.Interface, orID int64) ([]projectRow, error) {
	var prs []projectRow
	err := s.Table(projectTable).Find(&prs, &projectRow{OwnerID: orID, Visibility: int(visibility.Public)})
	if err != nil {
		return nil, err
	}

	return prs, nil
}

// listInternalProjectRowsForOwner returns the interal project rows of the given owner
func listInternalProjectRowsForOwner(s xorm.Interface, orID int64) ([]projectRow, error) {
	var prs []projectRow
	err := s.Table(projectTable).Find(&prs, &projectRow{OwnerID: orID, Visibility: int(visibility.Internal)})
	if err != nil {
		return nil, err
	}

	return prs, nil
}

// listPrivateProjectRows returns the private project rows of the given owner
func listPrivateProjectRows(s xorm.Interface, orID int64) ([]projectRow, error) {
	var prs []projectRow
	err := s.Table(projectTable).Find(&prs, &projectRow{OwnerID: orID, Visibility: int(visibility.Private)})
	if err != nil {
		return nil, err
	}

	return prs, nil
}

// getProjectRow returns the projectRow for the given projectID.
// The projectID contains the owner and the name of the project.
func getProjectRow(s xorm.Interface, pID id.ProjectID) (projectRow, bool, error) {
	oID, err := lookupOwnerRowID(s, pID.ActorID)
	if err != nil {
		return projectRow{}, false, err
	}

	pr := projectRow{OwnerID: oID, Name: pID.Project()}
	ex, err := s.Table(projectTable).Get(&pr)
	if err != nil || !ex {
		return projectRow{}, false, err
	}

	return pr, true, nil
}

// getProjectRowByID returns the projectRow for the given id.
// The id is the id in the database.
func getProjectRowByID(s xorm.Interface, projectID int64) (projectRow, bool, error) {
	pr := projectRow{ID: projectID}
	ex, err := s.Table(projectTable).Get(&pr)
	if err != nil || !ex {
		return projectRow{}, false, err
	}

	return pr, true, nil
}

func sortProjects(ps []*project.Project) {
	// sort the projects
	sort.Slice(ps, func(i, j int) bool {
		cmp := strings.Compare(ps[i].Name, ps[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}

// removeDuplicatesFromProjectRows removes all duplicates from the slice
func removeDuplicatesFromProjectRows(rows []projectRow) []projectRow {
	encountered := map[int64]bool{}
	var result []projectRow

	for _, row := range rows {
		if encountered[row.ID] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[row.ID] = true
			// Append to result slice.
			result = append(result, row)
		}
	}

	return result
}

func (psSQL ProjectsSQL) RenameProjectVersion(p *project.Project, oldVersionName, newVersionName string) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	err := renameSUTVersion(s, p, oldVersionName, newVersionName)
	if err != nil {
		return err
	}

	return s.Commit()
}

func (psSQL ProjectsSQL) RenameProjectVariant(p *project.Project, version, oldVariantName, newVariantName string) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	err := renameSUTVariant(s, p, version, oldVariantName, newVariantName)
	if err != nil {
		return err
	}

	return s.Commit()
}
