/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// IntegrationsPut handles updating the settings for the integration of
// a gitlab project
func IntegrationsPut(adder handler.ProjectAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.IsOwner(c.User) && !c.Project.GetPermissions(c.User).EditProject && !c.User.IsAdmin {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		id := strings.Trim(r.FormValue(httputil.GitlabProjectID), "")
		token := strings.Trim(r.FormValue(httputil.GitlabProjectToken), "")
		isActive, _ := handler.StringToBool(r.FormValue(httputil.GitlabIsActive))

		if isActive && (id == "" || token == "") {
			errors.ConstructStd(http.StatusBadRequest,
				"Invalid input",
				"Invalid project id or token", r).
				WithLog("unable to update project. invalid id or token").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		c.Project.Integrations.GitlabProject.ID = id
		c.Project.Integrations.GitlabProject.Token = token
		c.Project.Integrations.GitlabProject.IsActive = isActive

		if err := adder.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}
