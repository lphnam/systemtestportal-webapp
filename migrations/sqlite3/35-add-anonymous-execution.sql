-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
ALTER TABLE test_case_protocols ADD COLUMN is_anonymous BIT(1);
ALTER TABLE test_sequence_protocols ADD COLUMN is_anonymous BIT(1);

-- +migrate Down
ALTER TABLE test_case_protocols RENAME TO test_case_protocols_old;
CREATE TABLE test_case_protocols (
    id INTEGER PRIMARY KEY,
    protocol_nr INTEGER,
    test_case INTEGER NOT NULL REFERENCES test_cases(id) ON DELETE CASCADE,
    test_version INTEGER,
    test_sequence INTEGER,
    project_row_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    tester VARCHAR(255),
    sut_version VARCHAR(255),
    sut_variant VARCHAR(255),
    execution_date TIMESTAMP,
    result VARCHAR(255),
    comment VARCHAR(255),
    other_needed_time INTEGER
);
INSERT INTO test_case_protocols SELECT id, protocol_nr, test_case, test_version, test_sequence, project_row_id,
tester, sut_version, sut_variant, execution_date, result, comment, other_needed_time FROM test_case_protocols_old;
DROP TABLE test_case_protocols_old;

ALTER TABLE test_sequence_protocols RENAME TO test_sequence_protocols_old;
CREATE TABLE test_sequence_protocols (
    id INTEGER PRIMARY KEY,
    protocol_nr INTEGER,
    test_sequence INTEGER NOT NULL REFERENCES test_sequences(id) ON DELETE CASCADE,
    test_version INTEGER,
    project_row_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    result VARCHAR(255),
    tester VARCHAR(255),
    sut_version VARCHAR(255),
    sut_variant VARCHAR(255),
    execution_date TIMESTAMP,
    other_needed_time INTEGER
);
INSERT INTO test_sequence_protocols SELECT id, protocol_nr, test_sequence, test_version, project_row_id,
result, tester, sut_version, sut_variant, execution_date, other_needed_time FROM test_sequence_protocols_old;
DROP TABLE test_sequence_protocols_old;