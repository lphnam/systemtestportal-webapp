/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

const (
	errCanNotAddGroupTitle = "Group couldn't be created."
	errCanNotAddGroup      = "We are sorry but we were unable to create the group as you requested." +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + ". "
)

// groupInput contains the unvalidated data
// for the creation of a group
type groupInput struct {
	gn string
	gd string
	gv visibility.Visibility
	gm map[string]user.User
}

// GroupPost returns a function handling the creation of a group
func GroupPost(ga handler.GroupAdder, groupChecker id.ActorExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vis, err := getVisibilityFromRequest(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		gi := groupInput{
			gn: r.FormValue(httputil.GroupName),
			gd: r.FormValue(httputil.GroupDescription),
			gv: vis,
			gm: make(map[string]user.User),
		}

		g, err := newGroup(gi, ga, groupChecker, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, g.Name)
		w.WriteHeader(http.StatusOK)
	}
}

// getVisibilityFromRequest returns the visibility as Visibility type.
// Returns 0 if the visibility could not be read from the request.
// Applicable visibilities are public(1 as int), internal (2 as int) or
// private (3 as int).
func getVisibilityFromRequest(r *http.Request) (visibility.Visibility, error) {

	gvs := r.FormValue(httputil.GroupVisibility)
	gvis, err := visibility.StringToVis(gvs)
	if err != nil {
		return 0, handler.InvalidVisibility()
	}
	return gvis, nil
}

// newGroup tries to create and store a new group if an error
// occurs the returned group will be nil and an error will be returned.
func newGroup(gi groupInput, ga handler.GroupAdder, gc id.ActorExistenceChecker,
	r *http.Request) (*group.Group, error) {

	g := group.NewGroup(gi.gn, gi.gd, gi.gv, gi.gm)
	if vErr := g.ID().Validate(gc); vErr != nil {
		return nil, vErr
	}

	err := ga.Add(&g)
	if err != nil {
		return nil, errors.ConstructStd(http.StatusInternalServerError,
			errCanNotAddGroupTitle, errCanNotAddGroup, r).
			WithLog("Unable to add group.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}
	return &g, nil
}
