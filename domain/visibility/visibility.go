/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package visibility

import "errors"

// Visibility is the visibility of a group or a project
type Visibility int

// The visibility can be public, internal or private
const (
	Public Visibility = iota + 1
	Internal
	Private
)

// The visibilities as strings
const (
	PublicStr   = "public"
	InternalStr = "internal"
	PrivateStr  = "private"
)

// StringToVis converts a string to a visibility
func StringToVis(vis string) (Visibility, error) {
	switch vis {
	case PublicStr:
		return Public, nil
	case InternalStr:
		return Internal, nil
	case PrivateStr:
		return Private, nil
	default:
		return 0, errors.New("invalid string, only public, " +
			"internal or private are supported")
	}
}
