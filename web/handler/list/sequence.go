/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SequencesGet simply serves the page that lists sequences.
func SequencesGet(lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tss, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}


		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTestSequenceListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestSequences, tss).
			With(context.DeleteLabels, modal.LabelDeleteMessage), tmpl, w, r)
	}
}

func getTestSequenceListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesListFragment(r)
	}
	return getTabTestSequencesListTree(r)
}

// getTabTestSequencesListTree returns the test sequence list tab template with all parent templates
func getTabTestSequencesListTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence list tree
		Append(templates.TestSequencesList, templates.ManageLabels, templates.DeleteConfirm).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesListFragment returns only the test sequence list tab template
func getTabTestSequencesListFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestSequencesList, templates.ManageLabels, templates.DeleteConfirm).
		Get().Lookup(templates.TabContent)
}
