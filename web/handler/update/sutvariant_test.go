/*
This file is part of SystemTestPortal.
Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
package update

import (
	"testing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestProjectVariantsPut(t *testing.T) {
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	tooShortBody := "[" +
		"\"oldVersionName\"," +
		"]"

	versionBody := "[" +
		"\"oldVersionName\"," +
		"\"newVersionName\"" +
		"]"

	variantBody := "[" +
		"\"VersionName\"," +
		"\"oldVariantName\"," +
		"\"newVariantName\"" +
		"]"

	invalidBody := "öä?@[" +
		"\"VersionName\"," +
		"\"oldVariantName\"," +
		"\"newVariantName\"" +
		"]"

	unallowedVersionBody := "[" +
		"\"oldVersionName}}\"," +
		"\"newVersionName\"" +
		"]"

	unallowedVariantBody := "[" +
		"\"VersionName\"," +
		"\"oldVariantName}}\"," +
		"\"newVariantName\"" +
		"]"
	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0),
				)
			},
			handler.EmptyRequest(http.MethodPut),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, invalidBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, invalidBody),
		),
		handler.CreateTest("Version update case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 1),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, versionBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, versionBody),
		),
		handler.CreateTest("Variant update case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 1), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, variantBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, variantBody),
		),
		handler.CreateTest("unallowed version input case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, unallowedVersionBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, unallowedVersionBody),
		),
		handler.CreateTest("unallowed variant input case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, unallowedVariantBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, unallowedVariantBody),
		),
		handler.CreateTest("not right length input case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.ProjectSuTVersionUpdaterMock{}
				return ProjectVariantsPut(m), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(&m.ProjectAdderMock, 0),
					handler.HasCalls(&m.ProjectVersionRenamerMock, 0),
					handler.HasCalls(&m.ProjectVariantRenamerMock, 0), )
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, tooShortBody),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, tooShortBody),
		),
	)
}
