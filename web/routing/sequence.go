/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/assignment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/duplication"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/execution"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func registerSequencesHandler(pg *contextGroup, n *negroni.Negroni, testSequenceStore store.SequencesSQL) {

	testCaseStore := store.GetCaseStore()
	labelStore := store.GetLabelStore()
	activityStore := store.GetActivityStore()

	tssg := wrapContextGroup(pg.NewContextGroup(Sequences))

	tssg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.SequencesGet(testSequenceStore))))
	tssg.HandlerGet(New,
		n.With(negroni.WrapFunc(display.CreateSequenceGet(testCaseStore, testSequenceStore))))
	tssg.HandlerGet(Save,
		n.With(negroni.Wrap(defaultRedirect("../"))))
	tssg.HandlerGet(JSON,
		n.With(negroni.WrapFunc(json.SequencesGet(testSequenceStore))))
	tssg.HandlerGet(Info,
		n.With(negroni.WrapFunc(json.SequenceInfoGet(testCaseStore))))
	tssg.HandlerGet(Print,
		n.With(negroni.WrapFunc(printing.SequencesListGet(testSequenceStore))))

	tssg.HandlerPost(Save,
		n.With(negroni.WrapFunc(creation.SequencePost(testCaseStore, testSequenceStore, testSequenceStore, labelStore, activityStore))))

	registerSequenceHandler(tssg, n, testCaseStore, testSequenceStore)
}

func registerSequenceHandler(tssg *contextGroup, n *negroni.Negroni,testCaseStore store.CasesSQL, testSequenceStore store.SequencesSQL) {
	userStore := store.GetUserStore()

	commentStore := store.GetCommentStore()
	taskStore := store.GetTaskStore()
	activityStore := store.GetActivityStore()

	tsg := wrapContextGroup(tssg.NewContextGroup(VarSequence))

	tsg.HandlerGet(Show, n.With(middleware.TestSequence(testSequenceStore), negroni.WrapFunc(display.ShowSequenceGet(commentStore, taskStore, testSequenceStore))))
	tsg.HandlerPut(Show, n.With(middleware.TestSequence(testSequenceStore), negroni.WrapFunc(creation.SequenceCommentPut(commentStore))))
	tsg.HandlerGet(Edit,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(display.EditSequenceGet(testCaseStore, testSequenceStore))))
	tsg.HandlerGet(History,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(display.HistorySequenceGet)))
	tsg.HandlerGet(JSON,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(json.SequenceGet)))
	tsg.HandlerGet(Print,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(printing.SequenceGet)))

	tsg.HandlerPost(Duplicate,
		n.With(middleware.TestSequence(testSequenceStore), negroni.WrapFunc(duplication.SequencePost(testSequenceStore, testSequenceStore))))

	tsg.HandlerPut(Update,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(update.SequencePut(testCaseStore, testSequenceStore, testSequenceStore, commentStore, taskStore, testSequenceStore))))
	tsg.HandlerPut(Tester,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(assignment.SequencePut(userStore, taskStore, taskStore, taskStore, taskStore, activityStore))))

	tsg.HandlerDelete("",
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(deletion.SequenceDelete(testSequenceStore))))

	registerSequenceExecuteHandler(tsg, n, testSequenceStore, testCaseStore, testCaseStore, taskStore, taskStore, activityStore)
	registerSequenceLabelsHandler(tsg, n, testSequenceStore)
}

func registerSequenceExecuteHandler(tsg *contextGroup, n *negroni.Negroni,
	testSequenceStore middleware.TestSequenceStore, tcs middleware.TestCaseStore, caseLister handler.TestCaseLister,
	taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter, activityStore handler.Activities) {

	protocols := store.GetProtocolStore()
	projectStore := store.GetProjectStore()

	tsg.HandlerGet(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceStartPageGet(protocols))))
	tsg.HandlerPost(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceExecutionPost(protocols, protocols, protocols, tcs, caseLister,
				taskAdder, taskGetter, activityStore, projectStore, projectStore))))
	tsg.HandlerPut(Execute,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(execution.SequenceExecutionPut(protocols, protocols, tcs, protocols, taskAdder, taskGetter, activityStore))))
}

func registerSequenceLabelsHandler(tsg *contextGroup, n *negroni.Negroni, testSequenceStore store.SequencesSQL) {
	labelStore := store.GetLabelStore()

	labelContextGroup := wrapContextGroup(tsg.NewContextGroup(Labels))

	labelContextGroup.HandlerGet(Show,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(json.TestLabelsGet(labelStore))))
	labelContextGroup.HandlerPost(New,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(creation.TestLabelPost(labelStore))))
	labelContextGroup.HandlerPut(Delete,
		n.With(middleware.TestSequence(testSequenceStore),
			negroni.WrapFunc(deletion.DeleteTestLabelPut(labelStore))))
}
