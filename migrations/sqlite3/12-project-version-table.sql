-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
CREATE TABLE project_versions (
    id INTEGER PRIMARY KEY, 
    variant_id INTEGER NOT NULL REFERENCES project_variants(id) ON DELETE CASCADE, 
    name VARCHAR(255) NOT NULL,
    CONSTRAINT uniq_variant_id_name UNIQUE (variant_id, name)
);

-- +migrate Down
DROP TABLE project_versions;