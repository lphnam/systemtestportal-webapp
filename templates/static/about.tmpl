{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<div class="container">
    <div class="card p-4">
        <div class="row justify-content-center mb-4">
            <div class="col-4 col-sm-2 p-4">
                <img src="/static/img/STP-Logo.png" class="img-fluid image-responsive rounded">
            </div>
            <div class="col-12 col-sm-10 col-md-7">
                <h4>About:</h4>
                <p>{{T "SystemTestPortal was initially created by students of the University of Stuttgart as part of two large-scale study projects and supervised by employees of the Institute of Software Technology. Since then, it is run as a community project." .}}</p>
                <p>{{T "Both the initial and the current logo for SystemTestPortal were kindly contributed by" .}} <a href="https://gitlab.com/SilentTeaCup">Peter Werner</a>{{T "beigetragen" .}}</p>
                <p>{{T "SystemTestPortal is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version." .}}<p>

                <p>{{T "SystemTestPortal is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" .}}
                    <a href="/license">{{T "GNU General Public License" .}}</a> {{T "for more details." .}}</p>
                <a href="http://systemtestportal.org">
                    <button type="button" class="btn btn-primary">Website</button>
                </a><a href="https://gitlab.com/stp-team/systemtestportal-webapp" class="ml-2">
                <button type="button" class="btn btn-primary">Project on gitlab.com</button>
            </a>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-9">
                <h4>{{T "Used Libraries and tools:" .}}</h4>
                <table class="table table-responsive">
                    <tr>
                        <th>Category</th>
                        <th>Library</th>
                        <th>License</th>
                        <th>Link</th>
                    </tr>
                    <tr>
                        <td>Client Framework</td>
                        <td>Bootstrap</td>
                        <td><a href="https://github.com/twbs/bootstrap/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://http://getbootstrap.com/">getbootstrap.com</a></td>
                    </tr>
                    <tr>
                        <td>Client Framework Library</td>
                        <td>Bootstrap Multi-Select Plugin</td>
                        <td><a href="http://davidstutz.de/bootstrap-multiselect/#license">Apache License, Version 2.0, BSD 3-Clause License</a></td>
                        <td><a href="http://davidstutz.de/bootstrap-multiselect/">http://davidstutz.de/bootstrap-multiselect/</a></td>
                    </tr>
                    <tr>
                        <td>Client Framework Library</td>
                        <td>Bootstrap-Toggle</td>
                        <td><a href="https://github.com/minhur/bootstrap-toggle/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/minhur/bootstrap-toggle/">https://github.com/minhur/bootstrap-toggle/</a></td>
                    </tr>
                    <tr>
                        <td>Client Framework Library</td>
                        <td>Bootstrap-DatePicker</td>
                        <td><a href="https://github.com/atatanasov/gijgo/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="http://gijgo.com/datepicker/">http://gijgo.com/datepicker/</a></td>
                    </tr>
                    <tr>
                        <td>Framework</td>
                        <td>jQuery</td>
                        <td><a href="https://github.com/jquery/jquery/blob/master/LICENSE.txt">MIT License</a></td>
                        <td><a href="https://jquery.com//">jQuery.com</a></td>
                    </tr>
                    <tr>
                        <td>UI Framework</td>
                        <td>jQuery UI</td>
                        <td><a href="https://github.com/jquery/jquery-ui/blob/master/LICENSE.txt">jQueryUI License</a></td>
                        <td><a href="https://jqueryui.com///">jQueryUI.com</a></td>
                    </tr>
                    <tr>
                        <td>UI Framework</td>
                        <td>DataTables</td>
                        <td><a href="https://datatables.net/license/mit">MIT License</a></td>
                        <td><a href="https://datatables.net">datatables.net</a></td>
                    </tr>
                    <tr>
                        <td>UI Framework</td>
                        <td>HexColorPicker</td>
                        <td><a href="https://github.com/boonep/jquery-hex-colorpicker/blob/master/MIT-LICENSE.txt">MIT License</a></td>
                        <td><a href="http://www.booneputney.com/jquery-hex-colorpicker.html">www.booneputney.com</a></td>
                    </tr>
                    <tr>
                        <td>Client Icons</td>
                        <td>Font Awesome by Dave Gandy</td>
                        <td><a href="http://fontawesome.io/license/">MIT License / SIL OFL 1.1</a></td>
                        <td><a href="http://fontawesome.io/">fontawesome.io</a></td>
                    </tr>
                    <tr>
                        <td>Client Misc</td>
                        <td>Timeago</td>
                        <td><a href="https://github.com/rmm5t/jquery-timeago/blob/master/LICENSE.txt">MIT License</a></td>
                        <td><a href="http://timeago.yarp.com/">timeago.yarp.com</a></td>
                    </tr>
                    <tr>
                        <td>Server Toolkit</td>
                        <td>Gorilla</td>
                        <td><a href="https://github.com/gorilla/mux/blob/master/LICENSE">New BSD License</a></td>
                        <td><a href="http://www.gorillatoolkit.org">www.gorillatoolkit.org</a></td>
                    </tr>
                    <tr>
                        <td>Server Routing</td>
                        <td>httptreemux</td>
                        <td><a href="https://github.com/dimfeld/httptreemux/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/dimfeld/httptreemux">github.com/dimfeld/httptreemux</a></td>
                    </tr>
                    <tr>
                        <td>Server Middleware</td>
                        <td>negroni</td>
                        <td><a href="https://github.com/urfave/negroni/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/urfave/negroni">github.com/urfave/negroni</a></td>
                    </tr>
                    <tr>
                        <td>Configuration</td>
                        <td>congo</td>
                        <td><a href="https://gitlab.com/SilentTeaCup/congo/blob/master/LICENSE">MPL (Mozilla Public License)</a></td>
                        <td><a href="https://gitlab.com/SilentTeaCup/congo">gitlab.com/SilentTeaCup/congo</a></td>
                    </tr>
                    <tr>
                        <td>Error Tool</td>
                        <td>package errors</td>
                        <td> <a href="https://github.com/pkg/errors/blob/master/LICENSE">BSD 2-Clause "Simplified"</a></td>
                        <td><a href="https://github.com/pkg/errors">github.com/pkg/errors</a></td>
                    </tr>
                    <tr>
                        <td>Error Tool</td>
                        <td>go-multierror</td>
                        <td> <a href="https://github.com/hashicorp/go-multierror/blob/master/LICENSE">MPL (Mozilla Public License) 2.0</a></td>
                        <td><a href="https://github.com/hashicorp/go-multierror">github.com/hashicorp/go-multierror</a></td>
                    </tr>
                    <tr>
                        <td>PDF Creation</td>
                        <td>gofpdf</td>
                        <td> <a href="https://github.com/jung-kurt/gofpdf/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/jung-kurt/gofpdf">github.com/jung-kurt/gofpdf</a></td>
                    </tr>
                    <tr>
                        <td>ORM Mapper</td>
                        <td>go-xorm</td>
                        <td><a href="https://github.com/go-xorm/xorm/blob/master/LICENSE">BSD 3-Clause</a></td>
                        <td><a href="http://xorm.io/">xorm.io//</a></td>
                    </tr>
                    <tr>
                        <td>SQLite Driver</td>
                        <td>go-sqlite3</td>
                        <td><a href="https://mattn.mit-license.org/2012">MIT License</a></td>
                        <td><a href="http://mattn.github.io/go-sqlite3/">mattn.github.io/go-sqlite3/</a></td>
                    </tr>
                    <tr>
                        <td>SQL Migration</td>
                        <td>sql-migrate</td>
                        <td><a href="https://github.com/rubenv/sql-migrate/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/rubenv/sql-migrate/">github.com/rubenv/sql-migrate/</a></td>
                    </tr>
                    <tr>
                        <td>HTML Sanitizing</td>
                        <td>bluemonday</td>
                        <td><a href="https://github.com/microcosm-cc/bluemonday/blob/master/LICENSE.md">BSD 3-Clause License</a></td>
                        <td><a href="https://github.com/microcosm-cc/bluemonday/">github.com/microcosm-cc/bluemonday/</a></td>
                    </tr>
                    <tr>
                        <td>Markdown Parser</td>
                        <td>blackfriday</td>
                        <td><a href="https://github.com/russross/blackfriday/blob/master/LICENSE.txt">Simplified BSD License</a></td>
                        <td><a href="https://github.com/russross/blackfriday/">github.com/russross/blackfriday/</a></td>
                    </tr>
                    <tr>
                        <td>E-Mail</td>
                        <td>email</td>
                        <td><a href="https://github.com/jordan-wright/email/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/jordan-wright/email">github.com/jordan-wright/email</a></td>
                    </tr>
                    <tr>
                        <td>Monitoring Library</td>
                        <td>Prometheus</td>
                        <td><a href="https://github.com/prometheus/client_golang/blob/master/LICENSE">Apache License 2.0</a></td>
                        <td><a href="https://github.com/prometheus/client_golang/">github.com/prometheus/client_golang</a></td>
                    </tr>
                    <tr>
                        <td>Security Library</td>
                        <td>unrolled/secure</td>
                        <td><a href="https://github.com/unrolled/secure/blob/master/LICENSE">MIT License</a></td>
                        <td><a href="https://github.com/unrolled/secure/">github.com/unrolled/secure</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
{{end}}


