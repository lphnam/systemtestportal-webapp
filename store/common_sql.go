// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"fmt"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"log"
	"math/rand"
	"os"
	"path/filepath"

	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	"github.com/rubenv/sql-migrate"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	// Blank import to provide sqlite database driver
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity/activityItems"
)

const (
	ownerTable                        = "owners"
	userTable                         = "users"
	groupTable                        = "groups"
	projectTable                      = "projects"
	caseTable                         = "test_cases"
	caseVersionTable                  = "test_case_versions"
	casePreconditionTable             = "test_case_preconditions"
	sequencePreconditionTable         = "test_sequence_preconditions"
	stepTable                         = "test_steps"
	sequenceTable                     = "test_sequences"
	sequenceVersionTable              = "test_sequence_versions"
	sequenceVersionCaseTable          = "test_sequence_version_test_cases"
	sutVariantTable                   = "project_variants"
	sutVersionTable                   = "project_versions"
	caseVersionSUTVariantTable        = "test_case_version_project_variants"
	projectMembershipTable            = "project_membership_user"
	projectRolesTable                 = "project_roles"
	caseProtocolTable                 = "test_case_protocols"
	stepProtocolTable                 = "test_case_step_protocols"
	sequenceProtocolTable             = "test_sequence_protocols"
	caseProtocolPreconditionTable     = "test_case_protocols_precondition_results"
	sequenceProtocolPreconditionTable = "test_sequence_protocols_precondition_results"
	taskListTable                     = "todo_lists"
	taskItemTable                     = "todo_items"
	gitlabIssueTable                  = "gitlab_issues"

	idField            = "id"
	nameField          = "name"
	versionNumberField = "version_nr"
	stepIndexField     = "step_index"
	caseIndexField     = "case_index"
	taskItemIndexField = "todo_item_index"

	errorNoAffectedRows = "invalid number of affected rows (%d) when updating"
)

var engine *xorm.Engine

// InitializeDatabase initializes the database connection and makes sure the database schema is up to date
func InitializeDatabase() {
	engine = initDBEngine()

	migrateDB(engine)

	initContextDomain()
}

// GetActorExistenceChecker returns the implementation to use for ActorExistenceChecker
func GetActorExistenceChecker() OwnersSQL {
	return OwnersSQL{engine}
}

// GetUserStore returns a store for users
func GetUserStore() UsersSQL {
	return UsersSQL{engine}
}

// GetTaskStore returns a store for Tasks
func GetTaskStore() TaskSQL {
	return TaskSQL{engine}
}

// GetGroupStore returns a store for groups
func GetGroupStore() GroupSQL {
	return GroupSQL{engine}
}

// GetProjectStore returns a store for projects
func GetProjectStore() ProjectsSQL {
	return ProjectsSQL{engine}
}

// GetCaseStore returns a store for test cases
func GetCaseStore() CasesSQL {
	return CasesSQL{engine}
}

// GetSequenceStore returns a store for test sequences
func GetSequenceStore() SequencesSQL {
	return SequencesSQL{engine}
}

// GetProtocolStore returns a store for protocols
func GetProtocolStore() ProtocolsSQL {
	return ProtocolsSQL{engine}
}

func GetSystemSettingsStore() SystemSettingsSQL {
	return SystemSettingsSQL{engine}
}

func GetCommentStore() CommentsSQL {
	return CommentsSQL{engine}
}

func GetLabelStore() LabelsSQL {
	return LabelsSQL{engine}
}

func GetActivityStore() ActivitySQL {
	return ActivitySQL{engine }
}

func initDataDir() string {
	dir := config.Get().DataDir
	err := os.MkdirAll(dir, 0700)
	log.Printf("Loading sqlite database from: %s", dir)

	if err != nil {
		panic(err)
	}

	return dir
}

func initDBEngine() *xorm.Engine {
	dir := initDataDir()

	dbPath := filepath.Join(dir, "stp.db")
	connURL := fmt.Sprintf("file:%s?_foreign_keys=1", dbPath)
	e, err := xorm.NewEngine("sqlite3", connURL)

	if err != nil {
		panic(err)
	}

	e.SetMapper(core.GonicMapper{})

	return e
}

func migrateDB(e *xorm.Engine) {
	migrations := initMigrationSource()

	db := e.DB().DB

	applied, err := migrate.Exec(db, "sqlite3", migrations, migrate.Up)

	migrateDBForAutomatedXormStructs()

	log.Println("Applied", applied, "migrations")

	if err != nil {
		panic(err)
	}
}

func initMigrationSource() migrate.MigrationSource {
	migrationDir := filepath.Join(config.BasePath(), "migrations", "sqlite3")

	return &migrate.FileMigrationSource{
		Dir: migrationDir,
	}
}

/*
Automated XORM-Structs take advantage of XORMS available ORM-Functions.
*/
func migrateDBForAutomatedXormStructs() {
	err := engine.Sync2(new(system.SystemSettings))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(comment.Comment))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(project.Label))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(project.TestLabel))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(activity.Activity))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(activityItems.Assignment))
	if err != nil {
		panic(err)
	}

	err = engine.Sync2(new(activityItems.AssignedUsers))
	if err != nil{
		panic(err)
	}

	err = engine.Sync2(new(activityItems.TestCreation))
	if err != nil{
		panic(err)
	}

	err = engine.Sync2(new(activityItems.TestProtocol))
	if err != nil{
		panic(err)
	}
}

/*
SystemSettings are an globally accessible singleton. Therefore we must initialize it on system start.
*/
func initContextDomain() {

	// System Settings ------------------------------------------------------------|
	systemSettingsStore := GetSystemSettingsStore()
	systemSettings, err := systemSettingsStore.GetSystemSettings()
	if err != nil {
		panic(err)
	}

	contextdomain.SetGlobalSystemSettings(systemSettings)
}

// InitializeTestDatabase initializes an empty database for tests
func InitializeTestDatabase() {
	dbPath := fmt.Sprintf("file:db-%d?mode=memory&cache=shared&_foreign_keys=1;DateTimeKind=Utc", rand.Uint64())
	engine, _ = xorm.NewEngine("sqlite3", dbPath)
}
